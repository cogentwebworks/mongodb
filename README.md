## very small mongodb container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/mongodb.svg)](https://hub.docker.com/r/cogentwebs/mongodb/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/mongodb.svg)](https://hub.docker.com/r/cogentwebs/mongodb/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/mongodb.svg)](https://hub.docker.com/r/cogentwebs/mongodb/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/mongodb.svg)](https://hub.docker.com/r/cogentwebs/mongodb/)

This is a very small mongodb container but still have a working s6-overlay process and socklog-overlay .